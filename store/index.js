import cookie from 'cookie';
import { setAuthToken, resetAuthToken } from '~/utilis/auth';

export const state = () => ({
  sidebar: false
});
export const mutations = {
  toggleSidebar(state) {
    state.sidebar = !state.sidebar;
  }
};

export const actions = {
  nuxtServerInit({ dispatch }, context) {
    return new Promise((resolve, reject) => {
      const cookies = cookie.parse(context.req.headers.cookie || '');
      if ({}.hasOwnProperty.call(cookies, 'x-access-token')) {
        setAuthToken(cookies['x-access-token']);
        dispatch('auth/fetch')
          .then((result) => {
            resolve(true);
          })
          .catch((error) => {
            console.log('fetch user error', error);
            resetAuthToken();
            resolve(false);
          });
      } else {
        resetAuthToken();
        resolve(false);
      }
    });
  }
};
