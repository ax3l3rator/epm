export default function({ store, redirect, route }) {
  const userIsLoggedIn = !!store.state.auth.user;
  const urlRequiresNonAuth = {
    login: /^\/login(\/|$)/.test(route.fullPath),
    registration: /^\/register(\/|$)/.test(route.fullPath)
  };
  if (
    !userIsLoggedIn &&
    !urlRequiresNonAuth.login &&
    !urlRequiresNonAuth.registration
  ) {
    console.log('You are not logged in');
    console.log(!urlRequiresNonAuth.login, !urlRequiresNonAuth.registration);
    return redirect('/login');
  }
  if (
    userIsLoggedIn &&
    (urlRequiresNonAuth.login || urlRequiresNonAuth.registration)
  ) {
    console.log('You are logged in');
    console.log(urlRequiresNonAuth.login, urlRequiresNonAuth.registration);
    return redirect('/admin');
  }
  return Promise.resolve();
}
