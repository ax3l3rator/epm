CREATE DATABASE  IF NOT EXISTS `epm` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `epm`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: epm
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `columns`
--

DROP TABLE IF EXISTS `columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `columns` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `table_id` bigint DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `columns`
--

LOCK TABLES `columns` WRITE;
/*!40000 ALTER TABLE `columns` DISABLE KEYS */;
/*!40000 ALTER TABLE `columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invites`
--

DROP TABLE IF EXISTS `invites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invites` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `hash` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invites`
--

LOCK TABLES `invites` WRITE;
/*!40000 ALTER TABLE `invites` DISABLE KEYS */;
/*!40000 ALTER TABLE `invites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lobby`
--

DROP TABLE IF EXISTS `lobby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lobby` (
  `idInvites` bigint NOT NULL AUTO_INCREMENT,
  `Hash` varchar(64) DEFAULT NULL,
  `LobbyName` varchar(64) DEFAULT NULL,
  `Students` json DEFAULT NULL,
  `Deleted` binary(1) DEFAULT '0',
  PRIMARY KEY (`idInvites`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lobby`
--

LOCK TABLES `lobby` WRITE;
/*!40000 ALTER TABLE `lobby` DISABLE KEYS */;
INSERT INTO `lobby` VALUES (3,'31489056e0916d59fe3add79e63f095af3ffb81604691f21cad442a85c7be617','Test 3','[\"steve.balayan@gmail.com\"]',_binary '0'),(4,'73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049','New Table','[\"odsууу\"]',_binary '0'),(5,'3d914f9348c9cc0ff8a79716700b9fcd4d2f3e711608004eb8f138bcba7f14d9','New Table','[]',_binary '0');
/*!40000 ALTER TABLE `lobby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `table_id` bigint DEFAULT NULL,
  `name` varchar(64) DEFAULT 'New student',
  `surname` varchar(45) DEFAULT '0',
  `mark` tinyint unsigned DEFAULT '0',
  `likes` int DEFAULT '0',
  `dislikes` int DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `team_id` bigint DEFAULT NULL,
  `team_name` varchar(45) DEFAULT NULL,
  `team_color` varchar(7) DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (57,13,'New Student','0',4,3,0,1,'2020-06-03','2020-06-03',NULL,'None','#F44336',NULL),(58,13,'New Student','0',2,1,0,1,'2020-06-03','2020-06-03',NULL,'TEST','#4A148C',NULL),(59,13,'New Student','0',0,0,0,1,'2020-06-03','2020-06-03',NULL,'None','#F44336',NULL),(60,13,'New Student','0',2,9,0,0,'2020-06-03',NULL,NULL,'None','#F44336',NULL),(61,13,'New Student','0',0,0,0,0,'2020-06-03',NULL,NULL,'eone','#F44336',NULL),(62,14,'Vladimir Moskalenko','0',0,0,0,0,'2020-06-03',NULL,NULL,NULL,NULL,NULL),(63,14,'Alexey Koledaev','0',4,0,0,0,'2020-06-03',NULL,NULL,NULL,NULL,NULL),(64,15,'New Student','0',0,0,0,1,'2020-06-03','2020-06-03',NULL,NULL,NULL,NULL),(65,15,'New Student','0',0,0,0,1,'2020-06-03','2020-06-03',NULL,NULL,NULL,NULL),(66,15,'New Student','0',2,0,0,1,'2020-06-03','2020-06-03',NULL,NULL,NULL,NULL),(67,15,'New Student','0',0,0,0,1,'2020-06-03','2020-06-03',NULL,'None','#F44336',NULL),(68,15,'New Student','0',0,0,0,1,'2020-06-03','2020-06-03',NULL,NULL,NULL,NULL),(69,15,'New Student','0',0,0,0,1,'2020-06-03','2020-06-03',NULL,NULL,NULL,NULL),(70,15,'New Student','0',0,0,0,0,'2020-06-03',NULL,NULL,NULL,NULL,NULL),(71,15,'New Student','0',0,0,0,0,'2020-06-03',NULL,NULL,NULL,NULL,NULL),(72,15,'New Student','0',0,0,0,0,'2020-06-03',NULL,NULL,'None','#F44336',NULL),(73,16,'Moskalenko Volodimer','0',3,0,0,0,'2020-06-03',NULL,NULL,'Test Team','#AB47BC',NULL),(74,16,'Vanya Kurikhin','0',0,0,0,0,'2020-06-03',NULL,NULL,'Super Team','#FFB300',NULL),(75,16,'Koledaev Aleksey','0',2,7,3,0,'2020-06-03',NULL,NULL,'Just Team','#283593',NULL),(76,16,'Koledaev Aleksey','0',0,0,0,0,'2020-06-03',NULL,NULL,'Just Team','#F44336',NULL),(77,16,'Koledaev Aleksey','0',0,0,0,0,'2020-06-03',NULL,NULL,'Just Team','#F44336',NULL),(78,16,'Koledaev Aleksey','0',0,0,0,0,'2020-06-03',NULL,NULL,'Just Team','#F44336',NULL),(79,16,'Koledaev Aleksey','0',0,0,0,0,'2020-06-03',NULL,NULL,'Just Team','#F44336',NULL),(80,17,'Koledaev Aleksey','0',2,5,3,0,'2020-06-06',NULL,NULL,'test team','#23571F',NULL),(81,17,'New Student','0',5,0,0,1,'2020-06-06','2020-06-06',NULL,'test team','#F44336',NULL),(82,17,'Ivan Kurikhin','0',3,0,0,0,'2020-06-06',NULL,NULL,'Super Team','#F44336',NULL),(83,18,'New Student','0',3,0,0,1,'2020-06-06','2020-06-07',NULL,'Super Team','#F44336',NULL),(84,18,'New Student','0',4,0,0,1,'2020-06-06','2020-06-07',NULL,'Awesome Team','#673AB7',NULL),(85,18,'New Student','0',0,0,0,1,'2020-06-07','2020-06-07',NULL,'None','#F44336',NULL),(86,19,'New Student','0',0,1,0,0,'2020-06-07',NULL,NULL,'None','#F44336',NULL),(87,19,'New Student','0',0,0,0,0,'2020-06-07',NULL,NULL,'None','#F44336',NULL),(88,19,'New Student','0',0,0,0,0,'2020-06-07',NULL,NULL,'Non','#F44336',NULL),(89,21,'New Student','0',3,3,0,0,'2020-06-17',NULL,NULL,'None','#F44336',NULL),(90,23,'New Student','0',0,0,0,0,'2020-06-29',NULL,NULL,'None','#F44336',NULL),(91,25,'New Student','0',3,0,0,1,'2020-06-29','2020-07-16',NULL,'None','#F44336',NULL),(92,25,'New Student','0',0,4,0,0,'2020-06-29',NULL,NULL,'None','#F44336',NULL),(93,25,'New Student','0',0,0,0,0,'2020-06-30',NULL,NULL,'None','#F44336',NULL),(94,25,'New Student','0',0,0,0,0,'2020-07-03',NULL,NULL,'None','#F44336',NULL),(95,24,'New Student','0',0,0,0,0,'2020-07-04',NULL,NULL,'None','#F44336',NULL),(96,25,'New Student','0',0,0,0,0,'2020-07-07',NULL,NULL,'New Team','#F44336',NULL),(97,25,'New Student','0',0,0,0,0,'2020-07-07',NULL,NULL,'New Team','#F44336',NULL),(98,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(99,39,'New Student','0',2,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(100,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(101,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(102,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(103,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(104,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(105,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(106,39,'New Student','0',0,3,4,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(107,39,'New Studentww','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Teamw','#F44336',NULL),(108,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(109,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(110,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(111,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(112,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(113,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(114,39,'New Student','0',0,0,0,1,'2020-08-13','2020-08-13',NULL,'New Team','#F44336',NULL),(115,39,'New Student','0',3,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(116,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(117,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(118,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(119,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(120,39,'New Student','0',3,0,0,1,'2020-08-13','2020-09-13',NULL,'New Team','#F44336',NULL),(121,39,'New Student','0',0,2,0,1,'2020-08-13','2020-09-13',NULL,'New Team','#F44336',NULL),(122,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(123,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(124,39,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(125,39,'New Student','0',0,7,0,0,'2020-08-13',NULL,NULL,'New Team','#311B92',NULL),(126,40,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(127,40,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(128,40,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(129,40,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(130,40,'New Student','0',0,0,0,0,'2020-08-13',NULL,NULL,'New Team','#F44336',NULL),(131,40,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(132,40,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(133,40,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(134,41,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(135,41,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(136,41,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(137,45,'New Student','0',0,0,0,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(138,45,'Alex Ponk','0',0,0,0,1,'2020-08-14','2020-08-14',NULL,'Hello gay','#7B1FA2',NULL),(139,46,'New Student','0',0,6,2,0,'2020-08-14',NULL,NULL,'New Team','#F44336',NULL),(140,46,'New Student','0',0,0,0,1,'2020-08-14','2020-08-14',NULL,'New Team','#F44336',NULL),(141,39,'New Student','0',0,0,0,0,'2020-09-13',NULL,NULL,'New Team','#F44336',NULL),(142,39,'New Student','0',0,0,0,0,'2020-09-13',NULL,NULL,'New Team','#F44336',NULL),(143,47,'Grace Nam','0',5,0,0,0,'2020-10-02',NULL,NULL,'South Korea','#F44336',NULL),(144,48,'New Student','0',0,0,0,0,'2020-10-04',NULL,NULL,'Elzhbetta Bosak','#F44336',NULL),(145,49,'Grace Nam','0',0,0,0,0,'2020-10-06',NULL,NULL,'Korea','#F44336',NULL),(156,57,'Steve Balayan','0',5,0,0,0,'2020-10-11',NULL,NULL,NULL,NULL,50);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tables` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) DEFAULT '0',
  `name` varchar(45) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `created_id` bigint DEFAULT NULL,
  `hashID` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
INSERT INTO `tables` VALUES (23,0,'New Table',NULL,41,NULL),(24,0,'New Table',NULL,41,NULL),(25,1,'New Table','2020-07-16',39,NULL),(26,0,'New Table',NULL,NULL,NULL),(27,0,'New Table',NULL,NULL,NULL),(28,0,'New Table',NULL,NULL,NULL),(29,0,'New Table',NULL,NULL,NULL),(30,0,'New Table',NULL,NULL,NULL),(31,0,'New Table',NULL,NULL,NULL),(32,0,'New Table',NULL,NULL,NULL),(33,0,'New Table',NULL,NULL,NULL),(34,0,'New Table',NULL,NULL,NULL),(35,0,'New Table',NULL,NULL,NULL),(36,0,'New Table',NULL,NULL,NULL),(37,0,'New Table',NULL,NULL,NULL),(38,0,'New Table',NULL,NULL,NULL),(39,1,'New Table','2020-09-13',42,NULL),(40,1,'New Table','2020-09-13',42,NULL),(41,0,'New Table',NULL,42,NULL),(42,0,'New Table',NULL,42,NULL),(43,0,'New Table',NULL,42,NULL),(44,0,'New Table',NULL,42,NULL),(45,0,'ШКашкшр',NULL,42,NULL),(46,1,'Аккопк','2020-08-14',42,NULL),(47,0,'Test 3',NULL,7,NULL),(48,0,'Test 4',NULL,7,NULL),(49,0,'Test 5',NULL,7,NULL),(57,0,'Test 10',NULL,50,'c837649cce43f2729138e72cc315207057ac82599a59be72765a477f22d14a54');
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tasks` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `task` varchar(64) DEFAULT NULL,
  `comment` varchar(256) DEFAULT NULL,
  `tables_groups` json NOT NULL,
  `deleted` tinyint DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `completed_by` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (20,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03',NULL),(21,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03',NULL),(22,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03',NULL),(23,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03',NULL),(24,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03',NULL),(25,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03',NULL),(26,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',1,NULL,'2020-06-03','[57, 58]'),(27,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',0,NULL,NULL,'[60]'),(28,'New Task','Your comment','{\"groups\": null, \"tables\": [13]}',0,NULL,NULL,'[60, 61]'),(29,'Test Task','My awesome task; And your awesome mark','{\"groups\": null, \"tables\": [16]}',0,NULL,NULL,'[73, 74]'),(30,'New Task','Your comment','{\"groups\": null, \"tables\": [16]}',1,NULL,'2020-06-03',NULL),(31,'Test task','My comment, dont look at it. Ok?bla-bla','{\"groups\": null, \"tables\": [18]}',0,NULL,NULL,'[83, 84]'),(32,'Super Tasky','Your comment','{\"groups\": null, \"tables\": [18]}',1,NULL,'2020-06-06','[]'),(33,'task','Your comment','{\"groups\": null, \"tables\": [18]}',0,NULL,NULL,'[83, 84]'),(34,'New Task','Your comment','{\"groups\": null, \"tables\": [21]}',0,NULL,NULL,'[]'),(35,'New Task','Your comment','{\"groups\": null, \"tables\": [25]}',0,NULL,NULL,'[]'),(36,'effe','Your commentrter4gregerge','{\"groups\": null, \"tables\": [25]}',0,NULL,NULL,'[]'),(37,'New Task','Your commentмупукпупк','{\"groups\": null, \"tables\": [39]}',0,NULL,NULL,'[98, 99, 100]'),(38,'vsvd','vsvdsvdsvdsvdvsvsvssvvsd','{\"groups\": null, \"tables\": [45]}',0,NULL,NULL,'[137]');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `table_id` bigint DEFAULT NULL,
  `created_user_id` bigint DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uesrs`
--

DROP TABLE IF EXISTS `uesrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uesrs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uesrs`
--

LOCK TABLES `uesrs` WRITE;
/*!40000 ALTER TABLE `uesrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `uesrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(25) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'testy','$2b$15$uYqLt9qsiTzQi./PcYiNNeCuws55mQuCqFzMirXLeniNAVeOlr93e','2020-06-03 13:15:54',NULL,NULL),(8,'test','$2b$15$wY56F0zB.9ykf3/Okx0I1.YogFezzHPR2cvhRUGUWIU4H0gZhduuK','2020-06-03 13:17:15',NULL,NULL),(9,'test@test.test','$2b$15$Pn6DtoYZ3iZaCZTsO3ZYJO7ft27cdcvO0Zn5RCbejlj7X94.qG4Iy','2020-06-03 13:19:26',NULL,NULL),(10,'test@test.test','$2b$15$DCnLGRnyurr7NfGuun8hkuQsth0q/IEQ/K4WmMOb9dLTdd6X4wq3i','2020-06-03 13:21:29',NULL,NULL),(11,'test@email.com','$2b$15$iZIPolN74IXjtllrdxc5RuBgIkzayrVGbjftnZ6JOwdaPIcCGOkPy','2020-06-03 13:49:58',NULL,NULL),(12,'test@email.com','$2b$15$wTO7gXELkbPAqBYvn2ecLuOwg3JrAMhj.IrrOXZRHzwM.dhwnFfnC','2020-06-06 16:19:50',NULL,NULL),(13,'test@gmail.com','$2b$15$X1duFPVAI.0EMWu1ssJhQORkG.SrndX/xV3GzCiMZHFEXh88kSB2G','2020-06-17 14:49:46',NULL,NULL),(14,'testy','$2b$15$Kx2UWDB/emWWLMs.SWAn3.aPKcZVRZNluOM30LTnh..5Kjuvn1Yw2','2020-06-17 14:53:03',NULL,NULL),(15,'testfy','$2b$15$HVL8kP29YCQbP0cn8yg2uOPWf6tGif0y2Kd00J65LOYnKEsT9kYr6','2020-06-18 11:19:53',NULL,NULL),(16,'testfy','$2b$15$4s.IUZuABdC51mNVuGcGG.JedA9EXGL2U0iUXVCecDrpphlWT/ulm','2020-06-18 11:24:55',NULL,NULL),(17,'testfyd','$2b$15$SmOOF/QUBmM3RLEzZwubl.x/uLPuF/DC704ZA0s0ICT2nE6UJxlK2','2020-06-18 11:25:57',NULL,NULL),(18,'testfyd','$2b$15$1olVYLPMl8ACnVF98L4vKuaxy0zQhRYfp/FcxqwNZ9V4BkdMkgh5G','2020-06-18 11:40:26',NULL,NULL),(19,'qwerty','$2b$15$w46Qob.fbFGVwzqnY3rBHeLJ3WUyHLrfb4MBxOgwRumon3a.mpXJK','2020-06-18 11:42:48',NULL,NULL),(20,'admin','$2b$15$Hx1ao9flkXbT5IeC2HSGAe3eqV87IFkxLEL2XA43RxwvM.ZuWYE3O','2020-06-19 14:52:17',NULL,NULL),(21,'admin','$2b$15$qt/TZXbCvl0J/N32NzPm3.N/UDC6nAQSc753XUuRPSE/YdLhxBtuW','2020-06-19 14:53:18',NULL,NULL),(22,'admin','$2b$15$l2Y9JzALd/jwulMJ8vGpB.BvZKKzAlG3Rr3WQx0doQ.I6AAcDPqie','2020-06-19 14:54:45',NULL,NULL),(23,'admin','$2b$15$RO1A7IsPU.7pp1k1ZDhEEuB0SKyK187bCb.4nTcaFXK8FhCWLNX6.','2020-06-19 14:54:52',NULL,NULL),(24,'admin','$2b$15$PMwH/CxNp5r5deh8lGbIMOZnK.BVbq1ZkKi/Oy/qNIq.zcYLqdk7W','2020-06-19 14:55:59',NULL,NULL),(25,'admin','$2b$15$ul84q8RfaI.vcs5ZfSwUzO8kF4nv2ORHNoAR107HOQOqc3Ad7EBHG','2020-06-19 14:56:47',NULL,NULL),(26,'admin','$2b$15$VmsrkCBxVX82Ej85fz8Z9OUoir2SQWHAeYWfcPrbv9d1p/.wm90UG','2020-06-19 14:57:48',NULL,NULL),(27,'admin','$2b$15$LdzMDNOK3P7yIsANLHhMy.lu06pVaAJH19Ridzle/mdrV6ZreZVHC','2020-06-19 15:00:12',NULL,NULL),(28,'admin','$2b$15$4GN1sfOtYGeHBmy6pDHYHu1xkQkLQ7t9qwgESwwgM1XY37ECzomHi','2020-06-19 15:01:52',NULL,NULL),(29,'admin','$2b$15$xXjo5TUC0PQiQyYm9beUaeB4GZJwsQS60lM68LA08kTbMAlkYujte','2020-06-19 15:02:52',NULL,NULL),(30,'admin','$2b$15$Qxnng9B4h24Koo1o2OEtA.HtcF.VuzcA310Feo/K8sPgpg8pbzXBS','2020-06-19 15:03:42',NULL,NULL),(31,'admin','$2b$15$WTVkPYDN2R5uV.al9VlODOsdaTsIu6Qf8U6D4bYnK1un.ppozI1kO','2020-06-19 15:04:31',NULL,NULL),(32,'admin','$2b$15$WtgU6.fH1BggU.VmekcMZenUfrn/zIGnD0SshL1MimqrhsvpgQ9f.','2020-06-19 15:06:54',NULL,NULL),(33,'admin','$2b$15$cBeQmSlfFCTSuc9WvN3YtOKHZV7COy8e0niJNe3yj/UevTYn37sWG','2020-06-19 15:07:30',NULL,NULL),(34,'admin','$2b$15$l13VVikTsRKCQCd95MNW4O1.xaeTXatvTKKZ.OAcyED6M9L6TSXLu','2020-06-19 15:08:59',NULL,NULL),(35,'admin','$2b$15$G71J2cCFYlKP7kjsK5tWRuWnhk1Zouqa1fQEDtQRkVrVsPJirCGJi','2020-06-19 15:09:11',NULL,NULL),(36,'root','$2b$15$h3c91U7uL6ZNyEF4ADgxZOTWXZ1dqbkvBkXHV52E0zIzyCwJY./ia','2020-06-19 15:09:58',NULL,NULL),(37,'retortrgd','$2b$15$mgJHxzOLw2kAX1tNxYMW3uTBDxwy2naJI4NK8NqelmcOqnOIwijXC','2020-06-19 15:10:49',NULL,NULL),(38,'test@email.com','$2b$15$CWaVcsAUAmJPsbteUfDqTOlkSovgGs48HatPaIF7.hQL425dYtkXW','2020-06-28 12:30:34',NULL,NULL),(39,'ods','$2b$15$aMdWFybRwRHUV34PAkJF5e..OEvFEEcZ.L9x2I2YAEbI7Khi6O76u','2020-06-28 12:31:51',NULL,NULL),(40,'odsw','$2b$15$KkZwBTIeEu3bt8AxVGNrTu2aUTD2JzgUwTZAEboEo5HGOYxObf7SG','2020-06-28 12:32:01',NULL,NULL),(41,'oxygen','$2b$15$jgbvXgTUjM2TGPDNTjqs.ua3f4yEzVMxki6TQ.Uwfyw0mMrOkFZFS','2020-06-28 13:07:19',NULL,NULL),(42,'odsууу','$2b$15$.fw4vf8G2jZLYr6/um/iwen8hmsc/bb7hd4Kq9CYjjc9DLoPm.oqG','2020-08-11 13:55:41',NULL,NULL),(43,'FFFF','$2b$15$spy2w6SUzoWxsXpxNZkypefuJPVr07VmHkOJd24uXC0iqqFO0z2Sa','2020-10-10 13:40:33',NULL,NULL),(44,'FFFFf','$2b$15$MElaqo5IgUcrHk8veP0bLuJroBjdNtSIYzZBzMXcjNJKaZ8lYBWB2','2020-10-10 16:11:59',NULL,NULL),(45,'kkookoko','$2b$15$xCHcSEl3JiCkjzEo8Hi11eCwL.7ymsec/19Q49adDrcDWwXwrz.Da','2020-10-10 16:22:54',NULL,NULL),(46,'kkookokofe','$2b$15$PGl1r8csN5OtWfi4SkMtkerNzS8jK0QEaphFNIOWRF4B/EyxfQ0Ly','2020-10-10 16:25:22',NULL,NULL),(47,'kkookokofedw','$2b$15$olZcpSo3lvU7jC9Lf8FnTuQA1UMFsntcNtxsEfOjbULfBEpTHpmVW','2020-10-10 16:27:01',NULL,NULL),(48,'kkookokofedwdw','$2b$15$thh84oHu4IlRrdQKaAtlUuugui9ksWVuImU9n7DoS80qUKBu6BgBa','2020-10-10 16:27:33','dwdwwd','dwdw'),(49,'d','$2b$15$D6XPGP3J5v4wXbPa/WoYcOgmeFbdX3gYBBQ9U.bncJN4QeMA6kSdy','2020-10-10 16:32:35','',''),(50,'steve.balayan@gmail.com','$2b$15$mKCvRZjJZIr8W1ma.7pjoOOd8bQ7tTBrIKSAMWuE8PfaXZJ7QgJ3m','2020-10-10 19:48:27','Steve','Balayan');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vals`
--

DROP TABLE IF EXISTS `vals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vals` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `val` varchar(45) DEFAULT NULL,
  `likes` int DEFAULT '0',
  `dislikes` int DEFAULT '0',
  `deleted` tinyint DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `student_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vals`
--

LOCK TABLES `vals` WRITE;
/*!40000 ALTER TABLE `vals` DISABLE KEYS */;
/*!40000 ALTER TABLE `vals` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-11  0:35:04
