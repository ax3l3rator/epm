import axios from 'axios';

export default {
  auth: {
    me: () => axios.get('/me'),
    login: (data) => axios.post('/login', data),
    register: (data) => axios.post('/register', data)
  }
};
