const pool = require('./pool');

const get = () => {
  return new Promise((resolve, reject) => {
    pool.query(`SELECT * FROM tasks WHERE deleted != '1'`, (err, rows) => {
      if (rows == undefined) {
        reject('No tasks with this id');
      } else if (rows.length > 0) {
        resolve(rows);
      } else {
        reject('No tasks with this id');
      }
    });
  });
};
const add = (data, json) => {
  return new Promise((resolve, reject) => {
    data.tables_groups = json;
    pool.query(`INSERT INTO tasks SET ?`, data, (err, rows) => {
      console.log(err);
      resolve('Success');
    });
  });
};
const getbytbl = (table_id) => {
  return new Promise(async (resolve, reject) => {
    const all = await get().catch((err) => {
      reject('No tasks to this table');
    });

    const result_tasks = [];
    if (all != undefined) {
      for (task of all) {
        if (
          JSON.parse(task.tables_groups).tables.includes(parseInt(table_id))
        ) {
          result_tasks.push(task);
        }
      }
      if (result_tasks.length > 0) resolve(result_tasks);
      else {
        console.log('debil blyat');
        reject('No tasks to this table');
      }
    } else {
      reject(false);
    }
  });
};
const edit = (id, data) => {
  return new Promise(async (resolve, reject) => {
    console.log(data);
    pool.query(`UPDATE tasks SET ? WHERE id = ${id} `, data, (err, result) => {
      if (err) {
        console.log(err);
      }
      resolve('Success');
    });
  });
};
const complete = (student_id, data) => {
  return new Promise(async (resolve, reject) => {
    data.completed_by = JSON.stringify(data.completed_by);
    pool.query(
      `UPDATE tasks SET ? where id=${student_id}`,
      data,
      (err, result) => {
        if (err) {
          console.log(err);
        }
        resolve('Succes');
      }
    );
  });
};
const remove = (id) => {
  return new Promise((resolve, reject) => {
    data = { deleted_at: new Date(), deleted: true };
    pool.query(`UPDATE tasks SET ? WHERE id = ${id}`, data, (err, result) => {
      resolve('Success');
    });
  });
};
module.exports.get = get;
module.exports.add = add;
module.exports.getbytbl = getbytbl;
module.exports.complete = complete;
module.exports.edit = edit;
module.exports.remove = remove;
