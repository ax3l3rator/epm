const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtToken = require('../jwtToken');
const pool = require('./pool');
const saltRounds = 15;

const login = (username, password) => {
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM users WHERE email = \'${username}\'`,
      (err, suc) => {
        if (err) {
          reject(err);
        } else if (suc) {
          console.log(suc);
          const user = suc[0];
          bcrypt.compare(password, user.password, (error, result) => {
            if (result) {
              resolve({
                type: 'success',
                message: 'User loged in',
                user: { id: user.id, email: user.email },
                token: jwt.sign({ id: user.id, email: user.email }, jwtToken, {
                  expiresIn: '7d'
                })
              });
            } else {
              reject({ type: 'fail', message: 'Incorrect login or password' });
            }
          });
        }
      }
    );
  });
};
const register = async (uemail, pass, uname, usurname) => {
  const encryptedPassword = await bcrypt.hash(pass, saltRounds);
  const user = {
    email: uemail,
    password: encryptedPassword,
    name: uname,
    surname: usurname
  };
  return new Promise((resolve, reject) => {
    pool.query(`INSERT INTO users SET ?`, user, (err, succ) => {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve('Succesfully registered ');
      }
    });
  });
};

module.exports.login = login;
module.exports.register = register;
