/* eslint-disable */
const pool = require('./pool');
const tables = require('./tables');

const get = (id) => {
  return new Promise((resolve, reject) => {
    pool.query(`SELECT * FROM students where id = ${id}`, (err, rows) => {
      if (rows) {
        if (rows.length > 0) {
          console.log(err);
          resolve(rows);
        } else {
          reject('no students');
        }
      } else {
        reject('no students');
      }
    });
  });
};

const getTableStudents = (table_id) => {
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM students WHERE table_id = ${table_id} AND deleted != 1`,
      async (err, rows) => {
        if (rows) {
          resolve(rows);
        } else {
          reject('no students of table');
        }
      }
    );
  });
};

const update = (table_id, id, data) => {
  return new Promise((resolve, reject) => {
    tables
      .get(table_id)
      .then((table) => {
        pool.query(
          `UPDATE students SET ? WHERE id = ${id} AND table_id = ${table_id}`,
          data,
          (err, rows) => {
            resolve('Success');
          }
        );
      })
      .catch((err) => reject(err));
  });
};

const remove = (table_id, id) => {
  return new Promise((resolve, reject) => {
    tables
      .get(table_id)
      .then((table) => {
        const data = {
          deleted_at: new Date(),
          deleted: true
        };
        // rowas.removebystudent(table_id, id);
        pool.query(
          `UPDATE students SET ? WHERE id = ${id} AND table_id = ${table.id}`,
          data,
          (err, result) => {
            resolve(true);
          }
        );
      })
      .catch((err) => reject(err));
  });
};

// id == user_id при коннекте
// а это можно перегрузить?
const create = (table_id, data) => {
  return new Promise((resolve, reject) => {
    pool.query(`SELECT * from students WHERE deleted != 1 AND table_id = ${table_id} AND user_id = ${data.user_id}`, (err, res) => {
      if (res && res.length > 0) {
        reject('student has already been added');
      } else {
        tables
          .get(table_id)
          .then((table) => {
            // (data.created_at = new Date()), (data.deleted = false);
            // data.table_id = table_id;
            // data.user_id = id;
            // data.name == null ? 'new student' : data.name;
            pool.query(`INSERT INTO students SET ?`, data, (err, result) => {
              resolve(tables.get(table.id));
            });
          })
          .catch((err) => reject(err));
      }
    })
  });
};

module.exports.create = create;
module.exports.update = update;
module.exports.remove = remove;
module.exports.get = get;
module.exports.getTableStudents = getTableStudents;
